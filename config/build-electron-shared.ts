import path from "path";
import fs from "fs";
import esbuild from "esbuild";

function symlink(from: string, to: string) {
  const target = path.relative(path.dirname(from), to);
  try {
    fs.symlinkSync(target, from, "dir");
    console.log("ln", JSON.stringify(from), "->", JSON.stringify(to));
  } catch (err: unknown) {
    if (err instanceof Error && (err as NodeJS.ErrnoException).code !== "EEXIST") {
      console.error("error:", err.message);
      process.exit(1);
    }
  }
}

export function buildElectron(isProd: boolean) {
  try {
    const result = esbuild.buildSync({
      bundle: true,
      entryPoints: ["src/electron/index.ts", "src/electron/preload.ts"],
      outdir: isProd ? "build" : "dev",
      external: ["electron"],
      target: "chrome87",
      define: {
        "import.meta.env": JSON.stringify({
          DEV: !isProd,
          PACKAGE_VERSION: process.env.npm_package_version,
        }),
      },
      platform: "node",
      metafile: true,
    });

    if (result.warnings.length) {
      esbuild
        .formatMessagesSync(result.warnings, {kind: "warning"})
        .forEach(console.log);
    }

    if (result.metafile) {
      console.log(esbuild.analyzeMetafileSync(result.metafile));
    }

    if (!isProd) {
      symlink("dev/plugins", "src/electron/plugins");
    }
  } catch {
    process.exit(1);
  }
}
