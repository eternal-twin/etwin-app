import "electron";

declare module "electron" {
    interface WebContents {
        /**
         * Undocumented API.
         * See <https://github.com/electron/electron/issues/26929>.
         */
        destroy(): void;
    }
}
