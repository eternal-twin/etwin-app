import alphabounce from "./alphabounce.16.png";
import arkadeo from "./arkadeo.16.png";
import cafejeux from "./cafejeux.16.png";
import carapass from "./carapass.16.png";
import croquemonster from "./croquemonster.16.png";
import croquemotel from "./croquemotel.16.png";
import dinocard from "./dinocard.16.png";
import dinoparc from "./dinoparc.16.png";
import dinorpg from "./dinorpg.16.png";
import eternalfest from "./eternalfest.16.png";
import eternaltwin64 from "./eternaltwin.64.png";
import fever from "./fever.16.png";
import hammerfest from "./hammerfest.16.png";
import hammerfest64 from "./hammerfest.64.png";
import hordes from "./hordes.16.png";
import hyperliner from "./hyperliner.16.png";
import intrusion from "./intrusion.16.png";
import kadokado from "./kadokado.16.png";
import kingdom from "./kingdom.16.png";
import kube from "./kube.16.png";
import labrute from "./labrute.16.png";
import majority from "./majority.16.png";
import minitroopers from "./minitroopers.16.png";
import miniville from "./miniville.16.png";
import monsterhotel from "./monsterhotel.16.png";
import motionTwin64 from "./motion-twin.64.png";
import motionball from "./motionball.16.png";
import mush from "./mush.16.png";
import muxxu from "./muxxu.16.png";
import naturalchimie from "./naturalchimie.16.png";
import odyssey from "./odyssey.16.png";
import popotamo from "./popotamo.16.png";
import rockfaller from "./rockfaller.16.png";
import skywar from "./skywar.16.png";
import snake from "./snake.16.png";
import streetWriter from "./street-writer.16.png";
import studioquiz from "./studioquiz.16.png";
import teacherStory from "./teacher-story.16.png";
import twinoid64 from "./twinoid.64.png";

export {
    alphabounce,
    arkadeo,
    cafejeux,
    carapass,
    croquemonster,
    croquemotel,
    dinocard,
    dinoparc,
    dinorpg,
    eternalfest,
    eternaltwin64,
    fever,
    hammerfest,
    hammerfest64,
    hordes,
    hyperliner,
    intrusion,
    kadokado,
    kingdom,
    kube,
    labrute,
    majority,
    minitroopers,
    miniville,
    monsterhotel,
    motionTwin64,
    motionball,
    mush,
    muxxu,
    naturalchimie,
    odyssey,
    popotamo,
    rockfaller,
    skywar,
    snake,
    streetWriter,
    studioquiz,
    teacherStory,
    twinoid64
};
