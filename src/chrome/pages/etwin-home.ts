import cx from "classnames";
import m from "mithril";

import { GameGroup, GameMeta } from "../../common/etwinApi";
import { Versions } from "../../common/PreloadData";
import NewGif from "../assets/new.gif?url";
import Spinner from "../assets/spinner.svg";
import VersionSwf from "../assets/version.swf?url";
import { EtwinBox } from "../features/etwin-box";
import { EtwinHeader } from "../features/etwin-header";
import { EtwinFooter } from "../features/etwin-footer";
import { EtwinLink } from "../features/etwin-link";
import { EtwinMain } from "../features/etwin-main";
import { EtwinNewVersionButton } from "../features/etwin-new-version-button";
import { EtwinSection } from "../features/etwin-section";
import { EtwinTitle } from "../features/etwin-title";
import * as gameIcons from "../assets/games/index";

const appDlLink = "https://eternal-twin.net/docs/desktop";

export const EtwinHome = (): m.Component<{
    games: undefined | GameGroup[];
    versions: Versions;
    newVersion: null | string;
}> => ({
    view: ({ attrs }) => [
        EtwinHeader,
        m(EtwinMain, [
            m(EtwinSection, [
                attrs.newVersion &&
                    m(EtwinNewVersionButton, {
                        icon: NewGif,
                        text: "Etwin " + attrs.newVersion,
                        to: appDlLink,
                    }),
                m(EtwinTitle, { level: 1 }, "games"),
                attrs.games
                    ? m(
                          ".@md:columns-2",
                          attrs.games.map(group =>
                              m(GameGroup, { key: group.title, group }),
                          ),
                      )
                    : m(".w-1.h-1.mx-auto", Spinner),
            ]),
        ]),
        m(EtwinFooter, [
            m(EtwinBox, { class: "col-span-3" }, [
                m("dl", [
                    Object.entries(attrs.versions).map(([k, v]) => [
                        m("dt.etwin-fg-soft-purple.fw-bold.capitalize", k),
                        m("dd.ml-1/2.code", [
                            k === "flash"
                                ? attrs.versions.flash
                                    ? [m("p", attrs.versions.flash), FlashEmbed]
                                    : "<n/a>"
                                : v,
                        ]),
                    ]),
                ]),
            ]),
            contactBox,
        ]),
    ],
});

const contactBox = m(
    EtwinBox,
    m("ul.fw-bold", [
        [
            ["https://discord.gg/ERc3svy", "Discord server"],
            ["https://wiki.eternal-twin.net/", "Eternal Twinpedia"],
            ["https://gitlab.com/eternal-twin/etwin", "Gitlab repository"],
            ["https://eternal-twin.net/legal", "Legal notices"],
        ].map(([to, text]) => m("li", m(EtwinLink, { to }, text))),
    ]),
);

const GameGroup = (): m.Component<{
    group: GameGroup;
}> => ({
    view: ({ attrs }) =>
        m("article.inline-block.w-100%.mt-1/2", [
            m(EtwinTitle, { level: 2 }, [
                m(EtwinLink, { to: attrs.group.link }, [
                    m(Favicon, { size: 48, data: attrs.group }),
                    m("span.ml-1/2", attrs.group.title),
                ]),
            ]),
            m("ul.mt--1/5.etwin-ml-2,1rem", [
                attrs.group.items.map(game =>
                    m("li", { key: game.title }, m(GameMeta, { game })),
                ),
            ]),
        ]),
});

const GameMeta = (): m.Component<{
    game: GameMeta;
}> => ({
    view: ({ attrs }) => {
        const dead = attrs.game.icon === "dead";
        return m(
            EtwinLink,
            { to: attrs.game.link, disabled: dead },
            m(Favicon, { size: 16, data: attrs.game }),
            m("span.ml-1/5", attrs.game.title, dead && " (Offline)"),
        );
    },
});

const emptySvg = `data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg"/>`;

const Favicon = (): m.Component<{
    size: number;
    data: GameMeta | GameGroup;
}> => ({
    view: ({ attrs }) =>
        m("img.align-top.object-fit-contain", {
            class: cx({ crisp: !attrs.data.icon }),
            width: attrs.size,
            height: attrs.size,
            src: gameIcons[attrs.data.icon] ?? emptySvg,
        }),
});

const FlashEmbed = m.trust(`
    <embed type="application/x-shockwave-flash"
           src="${VersionSwf}"
           id="version"
           name="version"
           bgcolor="#ffffff"
           quality="high"
           allowscriptaccess="always"
           width="250"
           height="40">
`);
