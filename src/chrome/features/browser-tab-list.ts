import m from "mithril";

import IconAdd from "../assets/icon-add.svg";
import { autobind } from "../lib/autobind";
import { BrowserTab } from "./browser-tab";
import { UiButton } from "./ui-button";

export namespace BrowserTabList {
    type Tab = readonly [BrowserTab.State, BrowserTab.Actions];
    export type State = {
        tabs: Tab[];
        map: Map<string, Tab>;
        activeTab: Tab;
    };
    export type Actions = {
        newTab(o?: { type?: "background"; url?: string }): Tab;
        closeTab(id: string): boolean;
        setActiveTabId(id: string): void;
        setActiveOffset(id: number): void;
        findTabIndex(id: string): -1 | number;
        iterTab<T>(id: string, f: (tab: Tab) => T): T | undefined;
    };
}

const $BrowserTabList = `.
    .flex.items-center
    .h-1
    .pt-1/5.pr-1/10.pl-1/10
    .bg-gradient-bg0-80%-bg1
`;

export const BrowserTabList = (): m.Component<{
    state: BrowserTabList.State;
    actions: BrowserTabList.Actions;
}> => ({
    view: ({ attrs }) =>
        m($BrowserTabList, [
            attrs.state.tabs.map(tab =>
                m(BrowserTab, {
                    key: tab[0].id,
                    state: tab[0],
                    active: tab === attrs.state.activeTab,
                    handleClick: id =>
                        id !== attrs.state.activeTab[0].id &&
                        attrs.actions.setActiveTabId(id),
                    handleClose: attrs.actions.closeTab,
                }),
            ),
            m(UiButton, {
                icon: IconAdd,
                onclick: () => attrs.actions.newTab(),
                class: "mt--1/5 mr-1/10 ml-1/5 no-shrink",
            }),
        ]),
});

BrowserTabList.makeState = (): BrowserTabList.State => {
    const initialTab = BrowserTab.makeState();
    const actions = BrowserTab.makeActions(initialTab);
    const tab = [initialTab, actions] as const;
    return {
        tabs: [tab],
        map: new Map([[initialTab.id, tab]]),
        activeTab: tab,
    };
};

BrowserTabList.makeActions = (
    state: BrowserTabList.State,
): BrowserTabList.Actions =>
    autobind({
        newTab(o = {}) {
            const newState = BrowserTab.makeState();
            const newActions = BrowserTab.makeActions(newState);
            const newTab = [newState, newActions] as const;
            o.url && newActions.setUrl(new URL(o.url));
            state.tabs.push(newTab);
            state.map.set(newState.id, newTab);
            o.type !== "background" && (state.activeTab = newTab);
            return newTab;
        },

        closeTab(id) {
            // we don’t want to close the last tab
            if (state.tabs.length > 1) {
                const i = this.findTabIndex(id);
                if (i < 0) {
                    return false;
                }
                // close the tab
                state.tabs.splice(i, 1);
                state.map.delete(id);
                if (id === state.activeTab[0].id) {
                    // if the closed tab was the active one, set the
                    // right one as active if possible
                    this.setActiveTabId(
                        state.tabs[i - +(i >= state.tabs.length)][0].id,
                    );
                }
                return true;
            }
            return false;
        },

        setActiveTabId(id) {
            const tab = state.map.get(id);
            tab && (state.activeTab = tab);
        },

        setActiveOffset(offset) {
            const len = state.tabs.length;
            if (len < 2) {
                return;
            }
            // we can assume that it will always be found
            const i = this.findTabIndex(state.activeTab[0].id);
            let newIndex = (i + offset) % len;
            newIndex < 0 && (newIndex += len);
            this.setActiveTabId(state.tabs[newIndex][0].id);
        },

        findTabIndex(id) {
            for (let i = 0; i < state.tabs.length; ++i) {
                if (state.tabs[i][0].id === id) {
                    return i;
                }
            }
            return -1;
        },

        iterTab(id, f) {
            const tab = state.map.get(id);
            if (tab) {
                return f(tab);
            }
            return undefined;
        },
    });
