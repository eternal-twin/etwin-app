import m from "mithril";

const $Section = `section.
    .etwin-maxw-1024px
    .mt-3/2.mx-auto
    .p-1
    .b.bw-t-1/10.br-1/5.etwin-bc-red
    .etwin-bg-gray-70%
    .etwin-section-shadow
`;

export const EtwinSection = (): m.Component => ({
    view: ({ children }) => m($Section, children),
});
