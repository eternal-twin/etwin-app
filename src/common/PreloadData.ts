import { MainMessage, RendererMessage } from "./messages";

export type Versions = {
    etwin: string;
    electron: string;
    node: string;
    chrome: string;
    flash: string | null;
};

export type PreloadData = {
    versions: Versions;
    onmessage(handler: (message: MainMessage) => void): void;
    send(message: RendererMessage): void;
    env: "browser" | "electron";
};
